# 魔方还原程序

#### 介绍
使用python编写的魔方还原程序，分四个步骤还原魔方。
测试10000组随机打乱的魔方，平均还原步数33，最大还原步数41


#### 使用说明

1. import solve
2. solve.solve("RURFULBFBUBBDRRFDLDLLFFRLRRFUUDDLBUFDRRLLDDBDUBFBBUUFL")
3. 返回结果，例如：['U', "D'", 'F', 'U', 'R', 'B2', "R'", 'D2', 'L', "D'", 'L', 'U2', 'L2', 'U', 'R2', "U'", 'B2', 'U', 'L2', 'D', 'L2', 'L2', 'U2', 'L2', 'F2', 'R2', 'B2', 'D2', 'L2', 'U2', 'D2', 'B2', 'U2']


